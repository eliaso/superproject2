﻿using Cloudfuse.ModelsLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cloudfuse.ModelsLib.Budgeting
{
    public class Account : IMetadata
    {
        public enum AccountTypes { Checking, Cash}
        public Guid Id { get; set; }
        public string AccountName { get; set; }
        public AccountTypes Type { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        
    }
}
