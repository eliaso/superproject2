﻿using Cloudfuse.ModelsLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cloudfuse.ModelsLib.Budgeting
{
    public class Transaction:IMetadata
    {
        #region IMetadata
        public Guid Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; } 
        #endregion

        public string For { get; set; }
        public DateTime TransDate { get; set; }

        
    }
}
