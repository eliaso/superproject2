﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cloudfuse.ModelsLib.Budgeting
{
    public class Budget : Cloudfuse.ModelsLib.Interfaces.IMetadata
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string BudgetName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }


    }
}
