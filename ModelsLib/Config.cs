﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
namespace Cloudfuse.ModelsLib
{
    public static class Config
    {
        public static string MongoDbConn { get { return ConfigurationManager.AppSettings["mongoDbConn"]; } }
        public static string MongoDbName { get { return ConfigurationManager.AppSettings["mongoDbName"]; } }
    }
}
