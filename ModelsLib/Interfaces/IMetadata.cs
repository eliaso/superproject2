﻿using System;
namespace Cloudfuse.ModelsLib.Interfaces
{
    public interface IMetadata
    {
        Guid Id { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedDate { get; set; }
    }
}
