﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cloudfuse.ModelsLib.Interfaces
{
    public interface IGenericMongoRepository<T>
     where T : Cloudfuse.ModelsLib.Interfaces.IMetadata
    {
        void Delete(Guid id);
        T Get(Guid id, string username);
        IEnumerable<T> Get(string username);
        void Insert(T value);
        //Does an update/insert based on whether Guid has a value
        T Save(T value);
        void Update(Guid id, T value);
    }
}
