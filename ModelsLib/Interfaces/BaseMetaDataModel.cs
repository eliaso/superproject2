﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloudfuse.ModelsLib.Interfaces
{
    public class BaseMetaDataModel: IMetadata
    {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
    }
}
