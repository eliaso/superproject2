﻿using Cloudfuse.ModelsLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cloudfuse.ModelsLib.NonProfit
{
    public class OrganizationModel : IOrganization, IMetadata
    {
        public Guid Id{get;set;}
        public string Name { get; set; }
        public string Description{get;set;}
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}