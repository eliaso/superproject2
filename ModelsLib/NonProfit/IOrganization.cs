﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cloudfuse.ModelsLib.NonProfit
{
    public interface IOrganization
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
    }
}