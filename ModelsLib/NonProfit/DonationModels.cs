﻿using Cloudfuse.ModelsLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloudfuse.ModelsLib.NonProfit
{
    public class DonationModel: BaseMetaDataModel
    {
        public string ToOrgnanization { get; set; }
        public DateTime DateOfDonation { get; set; }
        public Decimal Amount { get; set; }
        public string DonorLogoImage { get; set; }
    }

    public class DonationModelWDepreciation : DonationModel
    {
        const double apr = 0.10;
        //private Decimal _depreciatedAmount ;
        public decimal DepreciatedAmount
        {

            get
            {
                decimal result = 0;

                if (DateOfDonation != DateTime.MinValue 
                    && Amount != 0)
                {
                    int daysDiff = (int)DateTime.Now.Subtract(DateOfDonation).TotalDays;
                    double dailyApr = apr / 365.0;
                    result = Math.Round(Amount / ((decimal)Math.Pow((1 + dailyApr), daysDiff)), 2);
                }
                return result;

            } 
        }
    }

    public class DonorRankingByDepreciatedTotal
    {
        public string DonorName { get; set; }
        public string DonorLogo { get; set; }
        public decimal DepreciatedTotal { get; set; }
        public decimal NominalTotal { get; set; }
        public DateTime FirstDonationDate { get; set; }
        public DateTime LastDonationDate { get; set; }
        public int NumberOfDonations { get; set; }

        //public IEnumerable<DonationModelWDepreciation> Details { get; set; }
    }
}
