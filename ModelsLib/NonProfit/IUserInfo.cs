﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cloudfuse.ModelsLib.NonProfit
{
    public interface IUserInfo
    {
        Guid Id { get; set; }
        string UserName { get; }
    }
}