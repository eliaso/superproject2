﻿namespace Cloudfuse.ModelsLib.Money
{
    public enum RoundingPlaces
    {
        Zero,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine
    }
}
