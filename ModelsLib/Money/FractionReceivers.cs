﻿namespace Cloudfuse.ModelsLib.Money
{
    public enum FractionReceivers
    {
        FirstToLast,
        LastToFirst,
        Random,
    }
}
