﻿namespace Cloudfuse.ModelsLib.Money
{
    public enum MidpointRoundingRule
    {
        ToEven,
        AwayFromZero,
        TowardZero,
        Up,
        Down,
        Stochastic
    }
}