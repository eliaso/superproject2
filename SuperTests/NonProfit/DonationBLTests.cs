﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloudfuse.BL.NonProfit;
using NUnit.Framework;
using Cloudfuse.ModelsLib.Money;
using Newtonsoft.Json;
using Cloudfuse.ModelsLib.NonProfit;
namespace Cloudfuse.BL.NonProfit.Tests
{
#if LOCAL
    [TestFixture()]
    public class DonationBLTests
    {

        private DonationModel CreateDonation()
        {
            var donation = new DonationModel
            {
                Amount = 10,
                ToOrgnanization = "YESSERA",
                DateOfDonation = DateTime.Now,
                CreatedBy = "TestUser",
                Name = "Acme Test Company",
                Description = "Earmarked as a test"
            };
            return new DonationBL().Save(donation);
        }

        [Test()]
        public void SaveTest()
        {
            var actual = CreateDonation();
            Assert.IsTrue(actual.Id != Guid.Empty, "When a donation is saved, it should have a non-null id");
            Assert.IsNotNullOrEmpty(actual.CreatedBy, "Created By should not be empty");
            Assert.IsTrue(actual.CreatedDate != DateTime.MinValue, "Created date must not be min.date");
            Assert.IsTrue(actual.Amount > 0, "Amount should be greater than zero");
            new DonationBL().Delete(actual.Id);
        }


        [Test()]
        public void GetDonationsByOrgNameTest()
        {
            //ensure at least one exists 
            var myDonation = CreateDonation();

            //run the test
            var bl = new DonationBL();
            var result = bl.GetDonationsByOrgName("YESSERA", "TestUser2");

            var formattedResult =
                JsonConvert.SerializeObject
               (
                    result,
                    new JsonSerializerSettings() { Formatting = Formatting.Indented }
                );
            Console.WriteLine(formattedResult);
            Assert.IsNotEmpty(result, "At least one result expected in your list of donation made to YESSERA");

            //ensure  the new donation is cleaned up
            bl.Delete(myDonation.Id);
        }

        [Test()]
        //[Ignore]
        public void DeleteDonationTest()
        {
            //ensure item to be deleted exists in db
            var newDonation = CreateDonation();

            var bl = new DonationBL();
            //ensure i can read it back from db
            var getFromDb = bl.Get(newDonation.Id, "me");
            Assert.IsNotNull(getFromDb, "I can get the current item back from db (id {0}).", newDonation.Id);


            //delete it and get it from db again
            bl.Delete(newDonation.Id);
            var getFromDb2 = bl.Get(newDonation.Id, "me");
            Assert.IsTrue(getFromDb2 == null, "After deleting item from db, i expect item to not exist.");
        }
    } 
#endif
}
