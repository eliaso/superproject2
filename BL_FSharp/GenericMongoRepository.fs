﻿namespace Cloudfuse.BL_Fsharp
    open Cloudfuse.ModelsLib
    open Cloudfuse.ModelsLib.Interfaces
    open Cloudfuse.ModelsLib.Budgeting
    open System
    open System.Linq
    open MongoDB.Driver
    open MongoDB.Driver.Builders

    type GenericMongoRepository<'T when 'T :> IMetadata> () =
        let cn = new MongoClient(Config.MongoDbConn)
        let _db = cn.GetServer().GetDatabase(Config.MongoDbName)
        let _collectionName =  typeof<'T>.Name
        let _collection = _db.GetCollection<'T>(_collectionName) 

        let Print(str:string) =
            System.Diagnostics.Debug.WriteLine(str)
            str

//        //todo: implement interface in F#
//        interface Cloudfuse.ModelsLib.Budgeting.IGenericMongoRepository<IMetadata> with
//            member this.Delete(id: Guid): unit = 
//                let bl = Cloudfuse.BL.GenericMongoRepository<'T>()
//                bl.Delete id
//                ()
//                //I can't figure out how to use query document object in F#
//                //
////               let query = new QueryDocument{{"_id", id}}
////               query.I
//               //query.And ([query.EQ((fun item -> item.Id), id)]) |> ignore
//              // _collection.Remove(query) |> ignore
//               //this is null ie unit type in f# speak!
//
//            member x.Get(id: Nullable<Guid>, username: string): Collections.Generic.IEnumerable<'T>  = 
//                let bl = Cloudfuse.BL.GenericMongoRepository<'T>()
//                bl.Get (id, username)
//            
//            member x.Insert(value: IMetadata): unit = 
//                failwith "Not implemented yet"
//            
//            member x.Save(value: IMetadata): unit = 
//                failwith "Not implemented yet"
//            
//            member x.Update(id: Guid, value: IMetadata): unit = 
//                failwith "Not implemented yet"
//             //with
//        
//        member public this.Get(id:Nullable<Guid>, username:string) = 
//            Print( String.Format("user: {0}", username)) 
//        member public this.Delete(id:Guid) =
//            Print "Deleting" 
//        member public this.Insert(value:'T) =
//            Print "inserting"
//        member public this.Save(id:Guid) = 
//            Print "Save"
