﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace SuperProj.Helper
{



 //   for this code is an article about claims authenticaiton on the kindle.
   public  interface IAuthenticationFilter: IFilter
    {
       Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken);
       Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken);
    }

   public class AuthenticationFilter : IFilter

   {
       public bool AllowMultiple
       {
           get { throw new NotImplementedException(); }
       }
   }
}
