﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cloudfuse.ModelsLib.NonProfit;

namespace SuperProj.Helper
{
    public class BaseAPIController : ApiController, IUserInfo
    {
        public Guid Id { get; set; }
        private string _username;
        public string UserName { get {
            //todo store ip address somewhere or use to figure out whether to give access
            //also store device id
            if (string.IsNullOrWhiteSpace(_username) && Request != null)
            {
                _username = Request.GetQueryNameValuePairs().Where(x => x.Key == "username").Select(x => x.Value).FirstOrDefault();
            }
            return _username; 
        } }

        private string _orgName;

        public string OrgName
        {
            get {
                if (string.IsNullOrWhiteSpace(_orgName) && Request != null)
                {
                    _orgName = Request.GetQueryNameValuePairs().Where(x => x.Key == "organization").Select(x => x.Value).FirstOrDefault();
                }
                return _orgName;

            }
            set { _orgName = value; }
        }
        
        public BaseAPIController()
        {
            if (Request != null && Request.RequestUri != null)
            {
                var parsedQuerystring = Request.RequestUri.ParseQueryString();
                if (parsedQuerystring != null)
                {
                    var usernameValues = parsedQuerystring.GetValues("username");
                    if (usernameValues != null)
                    {
                        _username = string.Join(",", usernameValues);
                    }
                }
            }
        }
    }
}
