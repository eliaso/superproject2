﻿


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Web;

namespace SuperProj.Helper
{
    public class WebAPiUserResolverFilterAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext!=null && actionContext.Request != null)
            {

                var username = actionContext.Request.GetQueryNameValuePairs().Where(x => x.Key == "username").Select(x=> x.Value).FirstOrDefault();
                if (username != "elias" && username != "yessera")
                    throw new UnauthorizedAccessException("Halt! who goes there.  Only authorized users can enter");
                else
                {

                    if (!UserOkToConnectFromIPAddress(username))
                    {
                        throw new UnauthorizedAccessException("Halt! who goes there.  Only authorized users can enter from expected ip addresses.");
                    }
                }
            }
        }

        //todo: low priority - limit ip addresses a user can connect from
        private bool UserOkToConnectFromIPAddress(string username)
        {
            #region Restrict caller ip address to specific ones
            //var myRequest = ((HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"]).Request;
            //foreach (var myKey in myRequest.ServerVariables.AllKeys)
            //{
            //    System.Diagnostics.Debug.WriteLine("{0}: {1}", myKey, myRequest.ServerVariables[myKey]);
            //    System.Diagnostics.Debug.WriteLine("--------- Attempt to Restrict caller ip address to specific ones --------");
            //}       
            #endregion
            return true;
        }

        //public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        //{
        //    //....
        //}
    }
}
