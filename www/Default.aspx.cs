﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SuperProj
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string envName = System.Configuration.ConfigurationManager.AppSettings["environmentName"];
             
            literal1.Text = string.Format(@"environment Name: {0}<br>
                 Build Date: {1}
                ", envName, Properties.Resources.BuildDate);
        }
    }
}