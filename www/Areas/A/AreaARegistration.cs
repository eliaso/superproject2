﻿using System.Web.Mvc;

namespace SuperProj.Areas.A
{
    public class AreaARegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "A";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TestArea_default1",
                "api2/A/{controller}/{id}",
                new { id = UrlParameter.Optional }
            );


        }
    }
}