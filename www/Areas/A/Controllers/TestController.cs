﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SuperProj.Areas.A.Controllers
{
    public class TestController : ApiController
    {
        // GET: api/WebAPI2
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WebAPI2/5
        public string Get(int id)
        {
            return "value";
        }

        public string Delete(string id)
        {
            return "You called the Delete method on: " + id;
        }

        //[HttpGet]
        //public string SayName() {
        //    return "your name";
        //}
    }
}
