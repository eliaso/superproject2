﻿using Cloudfuse.BL.NonProfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cloudfuse.ModelsLib.NonProfit;
using SuperProj.Helper;

namespace SuperProj.Controllers
{
    public class DonationController : BaseAPIController
    {
        //// GET: api/Donation
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}


        public IEnumerable<DonorRankingByDepreciatedTotal> GetByOrgName(string orgName)
        {
            var bl = new DonationBL();
            var result = bl.GetDonationsByOrgName(orgName, UserName);
            return result;
        }

        // POST: api/Donation
        public DonationModel Post([FromBody]DonationModel value)
        {
            var bl = new DonationBL();
            return bl.Save(value);
        }

        // PUT: api/Donation/5
        public void Put(Guid id, [FromBody]DonationModel value)
        {
            var bl = new DonationBL();
            value.Id = id;
            bl.Save(value);
        }

        // DELETE: api/Donation/5
        public void Delete(Guid id)
        {
            var bl = new DonationBL();
            bl.Delete(id);
        }
    }
}
