﻿using SuperProj.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SuperProj.Controllers
{
    public class OrganizationController : BaseAPIController
    {
        public OrganizationController():base()
        {
            //calls base constructor to hydrate username from querystring

        }

        // GET: api/Organization
        [WebAPiUserResolverFilter]
        public IEnumerable<string> Get()
        {
            return new string[] { "organization1" + UserName, "organization2" + UserName};
        }

        // GET: api/Organization/5
        public string Get(int id)
        {
            return "organization" + id + " username: " + UserName;
        }

        // POST: api/Organization
        public void Post([FromBody]string organization)
        {

        }

        // PUT: api/Organization/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Organization/5
        public void Delete(int id)
        {
        }
    }
}
