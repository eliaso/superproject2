﻿/// <reference path="~/Scripts/jquery-2.1.3.js" />
/// <reference path="~/Scripts/knockout-3.2.0.debug.js" />
/// <reference path="~/Scripts/knockout.mapping-latest.js" />
/// <reference path="~/Scripts/moment.js" />



$(function () {
    var util = {
        msg:function(str, obj1){
            var selector = $('#debug').find('.panel-body');
            selector.prepend(str + "<br>");
            if (obj1 !== undefined) {
                //selector.prepend("<br>");
                selector.prepend(JSON.stringify(obj1) + "<br>");
                console.debug(str);
                console.debug(obj1);
            }
        }
        ,bindKODateHandler: function() {
            ko.bindingHandlers.date = {
                update: function (element, valueAccessor) {
                    var value = valueAccessor();
                    var date = moment(value);
                    $(element).text(date.format("M/D/YY"));
                }
            };
        }
    };
    util.bindKODateHandler();

    function DonationViewModel() {
        var self = this;
        self.donations =
            // ko.observableArray(
             ko.mapping.fromJS([{
                 "DonorName": "Loading Ddonations...",
                 "DonorLogo": "/Content/images/csharp.png",
                 "DepreciatedTotal": 380.09,
                 "NominalTotal": 500.0,
                 "FirstDonationDate": "2012-05-01T10:07:46.904Z",
                 "LastDonationDate": "2012-05-01T10:07:46.904Z",
                 "NumberOfDonations": 1
             }]);

        
        self.newDonation = ko.mapping.fromJS({
            "DonorNameLabel": "Donor Name:",
            "DonorName": "a new donor",
            "DonorLogoLabel":"Dononor Logo:",
            "DonorLogo": "/Content/images/nestedDescriptions.png",
            "AmountLabel":"$:",
            "Amount": 20,
            "DateLabel": "Date:",
            "DonationDate": moment(new Date()).format('L'),
            "DescriptionLabel": "Notes",
            "Description": "Enter notes here..."
        });
        self.AddDonation_ShowForm = function (e, obj) {
            util.msg("<b>Hello from AddDonation_ShowForm</b>");
           $('#AddDonationForm').slideDown();
           obj.target.disabled = true;
           $('#show_addDonationButton').css('color', 'white')   ;
        };

        self.newDonation.AddDonation = function(e, obj) {
            util.msg("<b>Hello from AddDonation</b>");
            $('#show_addDonationButton').removeAttr("disabled");

            $('#show_addDonationButton').css('color', 'black');
            $('#AddDonationForm').slideUp('fast');
            var v = self.newDonation;
            var donorListValueInstance = {
                "DonorName": v.DonorName,
                "DonorLogo": v.DonorLogo,
                "DepreciatedTotal": v.Amount,
                "NominalTotal": v.Amount,
                "FirstDonationDate": new Date(),
                "LastDonationDate": new Date(),
                "NumberOfDonations": null
            };
            //self.donations.push(donorListValueInstance);

            //ko.utils.postJson('/Donation', v, function donationCallback(data) {

            //    util.msg('got data in donationCallback', data);

            //});
            ///$('#AddDonationForm').hideModal();
            var ajaxData = ({//JSON.stringify({ //ko.toJSON(v);
                "Name": v.DonorName(),
                "Description": v.Description(),
                "ToOrgnanization": "YESSERA", //todo refactor into the model somewhere
                "DateOfDonation": v.DonationDate(), //todo get from field in form
                "Amount": v.Amount(),
                "DonorLogoImage": v.DonorLogo()
            });
            util.msg("About to post this data: ", ajaxData);

            $.ajax({
                url: "/api/Donation",
                type: "POST",
                data: ajaxData,
                dataType: "json",
                //processData: false,
                //contentType: "application/json",
                success: function (data, textStatus, jqXHR) {
                    util.msg("Success callback: ", data);
                    donorsApp.loadDonors();
                },
                error: function errorCallback(result, status, errorThrown) {
                    util.msg("Error callback: ", result);
                    util.msg("status " + status);
                    util.msg("errorThrown: " + errorThrown);
                }
              
            });
        };

        return /*ko.mapping.fromJS*/(self);
    };
    var model = new DonationViewModel();

    var donorsApp = {
        loadDonors:function(){
            var getByOrgUrl = "/api/donation/GetByOrgName?username=elias&orgName=YESSERA";
            $.get(getByOrgUrl)
                .success(function(donors){
                    util.msg('Success. Data length: ' + donors.length, donors);

                    model.donations.splice(0,1);//remove all items from donations
                    for(var i=0,maxlen=donors.length;i<maxlen;i++) {
                        //model.donations.push(ko.mapping.fromJS(donor));
                        var donor = donors[i];
                        donor.DonorLogo = "/Content/images/csharp.png"; //todo remove hardwired logo image
                        //util.msg('pushing into donors: ' + typeof (donor), donor
                        model.donations.push(donor);

                    };
                })
                //.done(function(data) {
                //    util.msg('done. Data is: ', data);
                //})
                .fail(function(err) {
                    util.msg('Fail. Error is: ', err);
                })
                .always(function() {
                    util.msg('in .always');
                });
        }

    };
    ko.applyBindings(model);

    donorsApp.loadDonors();
});