﻿using System.Collections.Generic;
using System.Linq;
using Cloudfuse.ModelsLib.NonProfit;

namespace Cloudfuse.BL.NonProfit.Converters
{
    public static class DonationToDonationWDepreciation
    {

        public static IEnumerable<DonationModelWDepreciation> ToDonationModelWDepreciations
            (this IEnumerable<DonationModel> models)
        {
            return models.Select(m => new DonationModelWDepreciation()
            {
                Amount =  m.Amount,
                DateOfDonation = m.DateOfDonation,
                CreatedBy = m.CreatedBy,
                CreatedDate = m.CreatedDate,
                Description = m.Description,
                DonorLogoImage = m.DonorLogoImage,
                Id = m.Id,
                Name = m.Name,
                ToOrgnanization = m.ToOrgnanization
            });
        }

        public static IEnumerable<DonorRankingByDepreciatedTotal> ToDonorRankingByDepreciatedTotal
            (this IEnumerable<DonationModelWDepreciation> models)
        {
            return models
                .GroupBy(m=> m.Name)
                .Select(m => new DonorRankingByDepreciatedTotal
            {
                 DonorName =  m.Key,
                 DonorLogo =  m.Max(x=> x.DonorLogoImage),
                 DepreciatedTotal = m.Sum(x=> x.DepreciatedAmount),
                 NominalTotal = m.Sum(x=> x.Amount),
                 FirstDonationDate = m.Min(x=> x.DateOfDonation),
                 LastDonationDate = m.Max(x=> x.DateOfDonation),
                 NumberOfDonations = m.Count(),
                 //Details = m.Select(x=> x)
            })
            .OrderByDescending(x=> x.DepreciatedTotal);
        }
    }
}
