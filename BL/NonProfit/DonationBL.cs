﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloudfuse.BL.NonProfit.Converters;
using Cloudfuse.ModelsLib.NonProfit;
using Cloudfuse.ModelsLib;

namespace Cloudfuse.BL.NonProfit
{
    public class DonationBL : GenericMongoRepository<DonationModel>
    {
        public override DonationModel Save(DonationModel obj)
        {
            var err = "";
            if (obj == null)
                err = "Donation model must not be empty.";
            else
            {
                if (obj.Amount == 0)
                    err += "amount shouldn't be zero. ";
                if (string.IsNullOrEmpty(obj.Name))
                    err += "Name is required. ";
                if (obj.DateOfDonation == DateTime.MinValue)
                    //err += "A date is required. ";
                    obj.DateOfDonation = DateTime.Now;
            }
            if (string.IsNullOrWhiteSpace(err))
                return base.Save(obj);
            else
            {
                throw new Exception(err);
                //r err;
            }
        }

        public IEnumerable<DonorRankingByDepreciatedTotal> GetDonationsByOrgName(string orgName, string username)
        {
            //var repo = new GenericMongoRepository<DonationModel>();
            var coll = Util.GetMongoCollection<DonationModel>();

            var depreciatedDonations = coll.FindAll()
                    .Where(x => x.ToOrgnanization != null && x.ToOrgnanization.Equals(orgName, StringComparison.InvariantCultureIgnoreCase ))
                    .OrderByDescending(x => x.DateOfDonation)
                    .ToDonationModelWDepreciations();
            return depreciatedDonations.ToDonorRankingByDepreciatedTotal();
        }



        
    }
}
