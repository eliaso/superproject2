﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloudfuse.ModelsLib;
namespace Cloudfuse.BL
{
    public class Util
    {
        public static MongoCollection<T> GetMongoCollection<T> ()
        {
            var cn = new MongoClient(Config.MongoDbConn);
            var _db = cn.GetServer().GetDatabase(Config.MongoDbName);
            var _collectionName = typeof(T).Name;
            var _collection = _db.GetCollection<T>(_collectionName);
            return _collection;
        }
    }
}
