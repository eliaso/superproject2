﻿using Cloudfuse.ModelsLib;
using Cloudfuse.ModelsLib.Interfaces;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloudfuse.BL
{
    public class GenericMongoRepository<T> : 
        IGenericMongoRepository<T> 
        where T: IMetadata
    {
        //MongoDatabase _db;
        //string _collectionName;
        MongoCollection<T> _collection;
        public GenericMongoRepository()
        {
            //var cn = new MongoClient(Config.MongoDbConn);
            //_db = cn.GetServer().GetDatabase(Config.MongoDbName);
            //_collectionName = typeof(T).Name;
            //_collection = _db.GetCollection<T>(_collectionName);
            _collection =  Util.GetMongoCollection<T>();
        }

        public virtual T Get(Guid id, string username)
        {
            var result = _collection.FindOne(Query<T>.EQ(q=> q.Id, id));
            return result;
        }


        public virtual IEnumerable<T> Get(string username)
        {
            var result = _collection.FindAll()
                    .Where(x => x.CreatedBy == username);

            return result;
        }

        public virtual T Save(T value)
        {
            if (value.Id == Guid.Empty)
                Insert(value);
            else
                Update(value.Id, value);
            return value;
        }

        public virtual void Insert(T value)
        {
            if (value.CreatedDate == DateTime.MinValue)
                value.CreatedDate = DateTime.Now;
            _collection.Insert(value);
        }
        public virtual void Update(Guid id, T value)
        {
            var query = Query<T>.EQ(x => x.Id, id);
            var update = Update<T>.Set(x => x, value);
            _collection.Update(query, update);
        }

        public virtual void Delete(Guid id)
        {
            var query = Query<T>.EQ(x=> x.Id, id);
            _collection.Remove(query);
        }
    }
}
