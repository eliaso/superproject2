﻿* YESSERA Donations project
** Allow entering donations
*** querystring
    1. to org/cause
    2. date of donation
    3. amount 
** Get a list of donors and how much they contributed and when
*** endpointd:
**** Donation/GetByOrgName?orgName=YESSERA&sername=elias
     Gets depreciated amount by donor
**** api/organization?username=elias
     Gets list of organizations
** Get a list of depreciated donation amounts
/donation/GetByOrgName/?orgName=Yessera&username=elias
* TODO Authentication
** DONE Simple fox for now: claim based auth
** use filters to handle challenge and authorization
*** for example: http://localhost:52865/api/Organization?username=elias
*** article about webfilters http://msdn.microsoft.com/en-us/magazine/dn781361.aspx
** Enable authentication through mongoDB
* TypeScript to do javascript
** TODO emberJS:
*** deliver js methods to Kata/ember using typescript
**** Typescript should be able to use modules, but i get exception
     something about cannot have concrete implementations
* Nuget Plugins:
** [[https://github.com/InspectorIT/MongoDB.AspNet.Identity][MongoDB Role Provider]] Instructinos
** F# Power Tools, i installed it to get default interface implementation 
* TODO API in Areas:
** Sample url: http://localhost:53130/api/TestArea/User/1
** routing is confitured in /App_Start/WebApiConfig.cs
* TODO Katas
** Clien-side model binding frameworks
*** Ember
*** Angular
*** Knockout
